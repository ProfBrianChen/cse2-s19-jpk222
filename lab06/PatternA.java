////////////// CSE 002 Jake Kassis
//// Pattern A 
/// Lab06 3/8/19
// Import java scanner that is necessary for the lab
import java.util.Scanner; 
public class PatternA{
  // Main method required for starting every java program
  public static void main(String[] args){
    // Declare an instance of the scanner object
    Scanner userInput = new Scanner (System.in);
    // Prompt the user for their input between ints 1-10
    System.out.print("Enter number of rows in the range 1-10: ");
    // Check if the input is an integer
    while(userInput.hasNextInt() == false){
      // Create string junkInput to dispose of any incorrect input, similar to lab 05
      String junkInput = userInput.next();
      // Ask for valid input
      System.out.print("Error! Please enter a valid integer: ");
    }
    //Store the hopefully valid input
    int rowInput = userInput.nextInt();
    
    // Now, Check if the input is within the range of 1-10
    while(rowInput < 1 || rowInput > 10){
      //Prompt for an input within range
      System.out.print("Error! Please enter an integer within the range of 1-10: ");
      //Accept
      rowInput = userInput.nextInt();      
    }
    //Next, we have to print the number triangle using loops
    //Use noted from Prof Chens lecture
    //Keep in mind that the outer loop controls how many rows will be printed...use variables!!
    for (int numRows = 1; numRows <= rowInput; numRows++){
      //Inner loop to increment the numbers within the rows
      for (int rowValues = 1; rowValues <= numRows; rowValues++){
        //Print the contents of the rows
        System.out.print(rowValues + " ");
      }
      //Print the actual rows themselves
      System.out.println();
    }
           
  }
}
