//////////////
//// CSE 02 Arithmetic
///

public class Arithmetic{
  
  public static void main(String args []){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt 
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //Cost per belt
    double beltCost = 33.99;
    
    //The tax rate in state
    double paSalesTax = 0.06;
    
    
    
    //Total cost of pants
    double totalCostOfPants = 104.94;
    
    //Total cost of shirts
    double totalCostOfShirts = 49.98;
    
    //Total cost of belts
    double totalCostOfBelts = 33.99;
    
    //Total cost of clothes
    double totalCostOfClothes = 188.19;
    
    
   
   
    //Total cost of sales tax on pants
    double taxOnPants = 6.30;       //Actual 6.296399999999999
      
    //Total cost of sales tax on shirts
    double taxOnShirts = 2.30;     //Actual 2.9987999999999997
    
    //Total cost of sales tax on belts
    double taxOnBelts = 2.04;     //Actual 2.0394
    
    //Total cost of sales tax on clothes
    double taxOnClothes = 10.64;
    
    
    
    
    //The total cost of pants
    System.out.println("The total cost of pants would be = $" + numPants * pantsPrice);
    //The total cost of sweatshirts
    System.out.println("The total cost of sweatshirts would be = $" + numShirts * shirtPrice);
    //The total cost of belts
    System.out.println("The total cost of belts would be = $" + numBelts * beltCost);
    
    
    //Sales tax charged on all of the pants
    System.out.println("The total sales tax charged on all of the pants would be = $" + taxOnPants);
    //Sales tax charged on all of the sweatshirts
    System.out.println("The total sales tax charged on all of the sweatshirts would be = $" + taxOnShirts);
    //Sales tax charged on all of the belts
    System.out.println("The total sales tax charged on all of the belts would be = $" + taxOnBelts);
    
  
    //Total cost of purchase before tax
    System.out.println("The total cost of the entire purchase before tax would be = $" + (totalCostOfPants + totalCostOfShirts + totalCostOfBelts));
    
    //Total amount of sales tax
    System.out.println("The total cost of sales tax alone is = $" + (taxOnPants + taxOnShirts + taxOnBelts));
    
    //Total amount of purchase inlcuding sales tax
    System.out.println("The total cost of the purchase with tax is = $" + (totalCostOfClothes + taxOnClothes));
    
   
  }
}