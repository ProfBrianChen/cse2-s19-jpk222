////////////// Jake Kassis
//// CSE 02 CardGenerator
///
import java.util.Random;

public class CardGenerator{
  
  //Main method required for java priograms
  public static void main(String[] args){
    
    // Seeting the range to account for the amount of cards in the deck, which in a playing card deck is 52
    int cardPicked = (int)(Math.random() * (52) + 1);
    
    // As stated in lab 04, we need to make strings in order to identify the suit and identity of the card picked
    String cardIdentity = "";
    String cardSuit = "";
    
    // Creat an integer lim to keep the cards within their given ranges
    int limit; 
    
    // As stated in lab 04, use if statements to assign suits. Set in the confines of 1-13, 14-26, 27-39, 40-52. Use <= to include bounds!!
    if (1 <= cardPicked & cardPicked <=13)
      cardSuit = "Diamonds";// For Diamonds
      
      
    if (14 <= cardPicked & cardPicked <=26){
      cardSuit = "Clubs";// For Clubs
     
    }
    if (27 <= cardPicked & cardPicked <=39){
      cardSuit = "Hearts";//For Hearts
     
  }
    if (40 <= cardPicked & cardPicked <=52){
      cardSuit = "Spades";//For Spades
    
    }
    limit = cardPicked%13;
    // As stated in the lab 04, Use switch statement to assign an identity to the card. Cases 11-13 will represent royal cards
    switch(limit){ 
      case 0: cardIdentity = "Ace";
        break; 
      case 1: cardIdentity = "2";
        break; 
      case 2: cardIdentity = "3";
        break; 
      case 3: cardIdentity = "4";
        break;
      case 4: cardIdentity = "5";
        break; 
      case 5: cardIdentity = "6";
        break;
      case 6: cardIdentity = "7";
        break;
      case 7: cardIdentity = "8";
        break; 
      case 8: cardIdentity = "9";
        break; 
      case 9: cardIdentity = "10";
        break;
      case 10: cardIdentity = "Jack";
        break; 
      case 11: cardIdentity = "Queen"; 
        break; 
      case 12: cardIdentity = "King";
        break; }
    
    // Now, use a print statement to output the random card that was chosen
    System.out.println("You picked the " + cardIdentity + " of " + cardSuit);
      
    
    
    
    
    
  }
}