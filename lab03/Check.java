//////////////
//// CSE 02 Check
///

import java.util.Scanner;

public class Check{
  //main method required for every Java program
  public static void main(String[] args) {
    
    // You must declare an instance of the scanner object and call the Scanner constructor
    Scanner myScanner = new Scanner( System.in );
    
    // Prompt the user for the original cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
   
    //Used to accept user input
    double checkCost = myScanner.nextDouble();
    
    // Prompt the user for the tip percentage they wish to pay while also acceoting the input
    System.out.print("Enter the percentage tip that you wish to pay asa a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble ();
    tipPercent /= 100; // We want to convert the decimal percentage into a decimal value
      
      // Now, prompt the user for the number of people that went to dinner and accept the input. This vakue will represent the number of ways that the check will be split
      System.out.print("Enter the number of people who went out to dinner: ");
      int numPeople = myScanner.nextInt();
    
    // Now, just print out the output
    double totalCost;
    double costPerPerson;
    int dollars,  //Whole dollar amount of cost 
          dimes, pennies; //For storing digits
              //to the right of the decimal point
              //for the cost$
    totalCost = checkCost * (1+ tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount. e.g.,
    // (int) (6.73 * 10) % 10 -> 67 % 10 -> 7
    // where the % (mod) operator returns whatever is the remainder
    // after the division:   583%100 -> 83, 27%5 -> 2
    dimes=(int) (costPerPerson * 10) % 10;
    pennies=(int) (costPerPerson * 100) % 10;
    System.out.println("Ecah person in the group owes $" + dollars + '.' + dimes + pennies);
          
    
  } // End of main method
} // End of class
  
  