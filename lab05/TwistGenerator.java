////////////// CSE 002 Jake Kassis
//// Twist Generator
/// Lab 06

// Import Scanner that is necessary for the lab
import java.util.Scanner; 

public class TwistGenerator{
  //Main method required for every java program
  public static void main(String[] args){
     // Declare an instance of the scanner object
    Scanner userInput = new Scanner( System.in );
     // Prompt the user to input an integer for the length of our twist 
    System.out.print("Enter a positive integer for the length of the twist: ");
    //Now, we must implement our while loop
    while (userInput.hasNextInt() == false){ //As explained in lab sheet, hasNextInt returns true if what the user inputs is actually an integer
      //Create String junkWord to flush out whatever is entered into the scanner, it is useless in this context
      String junkWord = userInput.next();
      //Tell user that they need an actual integer
      System.out.println ("Sorry, please enter a valid integer: "); //For if user doesnt give one
      }
    //Store the new (hopefully valid) value entered as int length
    int Length = userInput.nextInt();
    
    //Check if input is positive. USe <=
    while (Length<=0){
      // Prompt for positive input
      System.out.print("Enter an integer that is positive, please: "); 
      //Accept input
      Length = userInput.nextInt(); 
        }
    // An integer to iterate will allow the twist to build
    // Lets use an iteration counter
    //Each vertical component of the column contains three chartacter
    //Lets set sperate columns to display each component
    
    //Column 1
    int column1 = 0; //Initalize!!
    
    //Column has to be less than or equal to the input length to work
    while (column1<Length){
      //Print out the three rows of the design acording to lab
      if(column1%3==0){
      System.out.print("\\");
      } //First Row //To print a single backslash, use 2
      else if(column1%3==1){
      System.out.print(" ");
      } //Second Row
      else{
      System.out.print("/");
      } //Third Row
      
        column1++; //Iterate until length is reached

        } // Do this process TW0 more times for the other columns
    
    System.out.println(); //Can use for a new line
    
    ////////////////////////////////////////////// Column 2
    int column2 = 0; //Initialize 2 as well
    
    //Column has to be less than or equal to the input length to work
    while (column2<Length){
      if(column2%3==0){
      System.out.print(" ");
      } //First Row 
      else if(column2%3==1){
      System.out.print("X");
      } //Second Row
      else{
      System.out.print(" ");
      } //Third Row
      
        column2++; //Iterate until length is reached
} // Do this process One more time for the other column
    
    
    System.out.println(); //For a new line
    
    ////////////////////////////////////////////// Column 3
    int column3 = 0; //Also initiatlize 3 
    
    //Column has to be less than or equal to the input  length to work
    while (column3<Length){
      if(column3%3==0){
      System.out.print("/");
      } //First Row //To print a single backslash, use 2
      else if(column3%3==1){
      System.out.print(" ");
      } //Second Row
      else{
      System.out.print("\\");
      } //Third Row
      
      
         column3++; //Iterate until length is reached
}
    
   System.out.println();   
      
  }
}
    
    