//////////////
//// CSE 02 Welcome Class
///

public class WelcomeClass{
  
  public static void main(String args[]){
    ///Prints the inteded design with each of its layers to the terminal window 
    System.out.println("     ----------");
      System.out.println("   | WELCOME |");
      System.out.println("   ----------");
      System.out.println("   ^   ^   ^   ^   ^   ^");
      System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<- J- -P- -K- -2- -2- -2->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("   v   v   v   v   v   v ");
      System.out.println("My name is Jake Kassis. I am from New York City.");
  }
}