//////////////
//// CSE 02 Cyclometer
///

public class Cyclometer{
  // main method required for every java program
  public static void main(String args[]){
    
    //our provided inout data for this lab
    int secsTrip1=480; //
    int secsTrip2=3220; //
    int countsTrip1=1561; //
    int countsTrip2=9037; //
    
    //variable created for useful constatnst and for storing various distances tht are given
    double wheelDiameter=27.0, //The given diameter of the wheel in this situation
    PI=3.14159, // A round up represenation of the value pi
    feetPerMile=5280, // The amount of feet that there are in a mile
    inchesPerFoot=12, // The amount of inches that there are in a foot
    secondsPerMinute=60; // The number of seconds there are in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //
    
      System.out.println("Trip 1 took "+
                          (secsTrip1/secondsPerMinute)+" minutes and had  "+
                          countsTrip1+" counts."); //Printing out the amount of time and counts in Trip 1
      System.out.println("Trip 2 took "+
                         (secsTrip2/secondsPerMinute)+ " minutes and had "+
                         countsTrip2+" counts."); //Printing out the amount of time and counts in Trip 2
    // Completion and storing of the calculated values
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //Above gives the distance in inches
    //For each count, a rotation of the wheel travels
    //the diameter in inches multiplied by PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives you the distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
      //Now, the output data must be printed.
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //Total distance for Trip 1
    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //Total distance for Trip 2
    System.out.println("The total distance was "+totalDistance+" miles"); //Total distance of both trips combined
    
  }
}
    
    
    
    
                         
            
                          