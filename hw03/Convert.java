//////////////
//// CSE 002 Convert
///

import java.util.Scanner;

public class Convert{
  //Main method required for every java program
  public static void main(String args[]){
    
    //For this homework assignment, I have implemented concepts from Lab 03
    //Must declare an instance of the scanner object and call the Scanner constructor
    Scanner myScanner = new Scanner( System.in );
    
    //Prompt the user for a distance in meters to subsequently convert to inches
    System.out.print("Enter the distance in meters: ");
    //Used to accept user input and do the actual conversion
    double distanceInMeters = myScanner.nextDouble();
    double distanceInInches = distanceInMeters * 39.3701;
    //Prints out the converison successfully
    System.out.print(distanceInMeters + " Meters is " + distanceInInches + " inches ");
    
  }
}
    