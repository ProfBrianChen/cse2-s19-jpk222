////////////// 
//// CSE 02 Box Volume
///

import java.util.Scanner;

public class BoxVolume{
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //You must declare an instance of the scanner object and call the Scanner constructor
    Scanner myScanner = new Scanner( System.in );
    //Prompts the user to input the correct data for the width side of the box
    System.out.print("The width side of the box is: ");
    
    //Used to accept user input
    double widthSide = myScanner.nextDouble();
    
    //Prompts the user to input the correct data for the length side of the box
    System.out.print("The length side of the box is: ");
    double lengthSide = myScanner.nextDouble ();
    
    //Prompts the iser to input the correct data for the height of the box
    System.out.print("The height side of the box is: ");
    double heightSide = myScanner.nextDouble ();
    
    //Now, we want to print out the volume inside of the box
    System.out.println("The volume inside the box is: " + (widthSide * lengthSide * heightSide));
    
  }//End of main method
 
}//End of class
