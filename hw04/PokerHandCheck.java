////////////// Jake Kassis
//// CSE 002 HWo4
///

import java.util.Random;
public class PokerHandCheck{
  
  // Main method required for every java prgram
  public static void main (String[] args){
    
    //Setting the range of for the amount of card in the deck, which in a playing card deck is 52, but do 5 times for 5 cards!
    int cardPicked1 = (int)(Math.random() * (52) + 1);
    int cardPicked2 = (int)(Math.random() * (52) + 1);
    int cardPicked3 = (int)(Math.random() * (52) + 1);
    int cardPicked4 = (int)(Math.random() * (52) + 1);
    int cardPicked5 = (int)(Math.random() * (52) + 1);
    
    //Similarly to lab04, we have to ge generate 5 strings for both the identity and suit of the cards
    String cardIdentity1 = "";
    String cardIdentity2 = "";
    String cardIdentity3 = "";
    String cardIdentity4 = "";
    String cardIdentity5 = "";
    
    String cardSuit1 = "";
    String cardSuit2 = "";
    String cardSuit3 = "";
    String cardSuit4 = "";
    String cardSuit5 = "";
    
    //Create an integer to keep the cards withing their given ranges. But do this for 5 cards
    int limit1;
    int limit2; 
    int limit3; 
    int limit4;
    int limit5;
    
    // (Card 1) Now, we must use if statements to assign suits to the cards. Set in the confines of 1-13, 14-26, 27-39, 40-52. Include bounds!
    if (1 <= cardPicked1 & cardPicked1 <=13)
      cardSuit1 = "Diamonds";//For Diamonds
    if (14 <= cardPicked1 & cardPicked1 <=26){
      cardSuit1 = "Clubs";//For CLubs
    }
    if (27 <= cardPicked1 & cardPicked1 <=39){
      cardSuit1 = "Hearts";//For Clubs
    }
    if (40 <= cardPicked1 & cardPicked1 <=52){
      cardSuit1 = "Spades";//For Spades
    }
    limit1 = cardPicked1%13;//To enforce boundaries
    
    ////////////////////////////////////////////// (Card 2) Below
    
    if (1 <= cardPicked2 & cardPicked2 <=13)
      cardSuit2 = "Diamonds";//For Diamonds
    if (14 <= cardPicked2 & cardPicked2 <=26){
      cardSuit2 = "Clubs";//For Clubs
    }
    if (27 <= cardPicked2 & cardPicked2 <=39){
      cardSuit2 = "Hearts";//For Hearts
    }
    if (40 <= cardPicked2 & cardPicked2 <=52){
      cardSuit2 = "Spades";//For Spades
    }
    limit2 = cardPicked2%13;//To enforce boundaries
    
    ////////////////////////////////////////////// (Card 3) Below
    
    if (1 <= cardPicked3 & cardPicked3 <=13)
      cardSuit3 = "Diamonds";//For Diamonds
    if (14 <= cardPicked3 & cardPicked3 <=26){
      cardSuit3 = "Clubs";//For Clubs
    }
    if (27 <= cardPicked3 & cardPicked3 <=39){
      cardSuit3 = "Hearts";//For Hearts
    }
    if (40 <= cardPicked3 & cardPicked3 <=52){
      cardSuit3 = "Spades";//For Spades
    }
    limit3 = cardPicked3%13;//To enforce boundaries
    
    ////////////////////////////////////////////// (Card 4) Below
    
    if (1 <= cardPicked4 & cardPicked4 <=13)
      cardSuit4 = "Diamonds";//For Diamonds
    if (14 <= cardPicked4 & cardPicked4 <=26){
      cardSuit4 = "Clubs";//For Clubs
    }
    if (27 <= cardPicked4 & cardPicked4 <=39){
      cardSuit4 = "Hearts";//For Hearts
    }
    if (40 <= cardPicked4 & cardPicked4 <=52){
      cardSuit4 = "Spades";//For Spades
    }
    limit4 = cardPicked4%13;//To enforce boundaries
    
    ////////////////////////////////////////////// (Card 5) Below
    
    if (1 <= cardPicked5 & cardPicked5 <=13)
      cardSuit5 = "Diamonds";//For Diamonds
    if (14 <= cardPicked5 & cardPicked5 <=26){
      cardSuit5 = "Clubs";//For Clubs
    }
    if (27 <= cardPicked5 & cardPicked5 <=39){
      cardSuit5 = "Hearts";//For Hearts
    }
    if (40 <= cardPicked5 & cardPicked5 <=52){
      cardSuit5 = "Spades";//For Spades
    }
    limit5 = cardPicked5%13; //To enforce boundaries
    
    ///////////////////////////////////////////////// (Card 1) Now We have to do the switch statement to assign indentity to cards 1-5
    switch(limit1){ //Always include breaks as well
      case 0: cardIdentity1 = "Ace";
        break;
      case 1: cardIdentity1 = "2";
        break;
      case 2: cardIdentity1 = "3";
        break;
      case 3: cardIdentity1 = "4";
        break; 
      case 4: cardIdentity1 = "5";
        break;
      case 5: cardIdentity1 = "6";
        break; 
      case 6: cardIdentity1 = "7";
        break;
      case 7: cardIdentity1 = "8";
        break; 
      case 8: cardIdentity1 = "9";
        break;
      case 9: cardIdentity1 = "10";
        break; 
      case 10: cardIdentity1 = "Jack";//No card for 11-13, so 11-13 represent royalty cards
        break;
      case 11: cardIdentity1 = "Queen";
        break;
      case 12: cardIdentity1 = "King";
        break;}
    
    ///////////////////////////////////////////////// (Card 2) Same as card 1. Continue to start with case 0
     switch(limit2){
      case 0: cardIdentity2 = "Ace";
        break;
      case 1: cardIdentity2 = "2";
        break;
      case 2: cardIdentity2 = "3";
        break;
      case 3: cardIdentity2 = "4";
        break; 
      case 4: cardIdentity2 = "5";
        break;
      case 5: cardIdentity2 = "6";
        break; 
      case 6: cardIdentity2 = "7";
        break;
      case 7: cardIdentity2 = "8";
        break; 
      case 8: cardIdentity2 = "9";
        break;
      case 9: cardIdentity2 = "10";
        break; 
      case 10: cardIdentity2 = "Jack";//No card for 11-13, so 11-13 represent royalty cards
        break;
      case 11: cardIdentity2 = "Queen";
        break;
      case 12: cardIdentity2 = "King";
        break;}
    
     ///////////////////////////////////////////////// (Card 3)
    switch(limit3){
      case 0: cardIdentity3 = "Ace";
        break;
      case 1: cardIdentity3 = "2";
        break;
      case 2: cardIdentity3 = "3";
        break;
      case 3: cardIdentity3 = "4";
        break; 
      case 4: cardIdentity3 = "5";
        break;
      case 5: cardIdentity3 = "6";
        break; 
      case 6: cardIdentity3 = "7";
        break;
      case 7: cardIdentity3= "8";
        break; 
      case 8: cardIdentity3 = "9";
        break;
      case 9: cardIdentity3 = "10";
        break; 
      case 10: cardIdentity3 = "Jack";//No card for 11-13, so 11-13 represent royalty cards
        break;
      case 11: cardIdentity3 = "Queen";
        break;
      case 12: cardIdentity3 = "King";
        break;}
    
    ///////////////////////////////////////////////// (Card 4)
     switch(limit4){
      case 0: cardIdentity4 = "Ace";
        break;
      case 1: cardIdentity4 = "2";
        break;
      case 2: cardIdentity4 = "3";
        break;
      case 3: cardIdentity4 = "4";
        break; 
      case 4: cardIdentity4 = "5";
        break;
      case 5: cardIdentity4 = "6";
        break; 
      case 6: cardIdentity4 = "7";
        break;
      case 7: cardIdentity4 = "8";
        break; 
      case 8: cardIdentity4 = "9";
        break;
      case 9: cardIdentity4 = "10";
        break; 
      case 10: cardIdentity4 = "Jack";//No card for 11-13, so 11-13 represent royalty cards
        break;
      case 11: cardIdentity4 = "Queen";
        break;
      case 12: cardIdentity4 = "King";
        break;}
    
     ///////////////////////////////////////////////// (Card 5)
     switch(limit5){
      case 0: cardIdentity5 = "Ace";
        break;
      case 1: cardIdentity5 = "2";
        break;
      case 2: cardIdentity5 = "3";
        break;
      case 3: cardIdentity5 = "4";
        break; 
      case 4: cardIdentity5 = "5";
        break;
      case 5: cardIdentity5 = "6";
        break; 
      case 6: cardIdentity5 = "7";
        break;
      case 7: cardIdentity5 = "8";
        break; 
      case 8: cardIdentity5 = "9";
        break;
      case 9: cardIdentity5 = "10";
        break; 
      case 10: cardIdentity5 = "Jack";//No card for 11-13, so 11-13 represent royalty cards
        break;
      case 11: cardIdentity5 = "Queen";
        break;
      case 12: cardIdentity5 = "King";
        break;}
    ///////////////////////// Print out the results of which cards you were dealt
    
    System.out.println("Your random cards are: ");
    
    System.out.println("the " + cardIdentity1 + " of " + cardSuit1);
    System.out.println("the " + cardIdentity2 + " of " + cardSuit2);
    System.out.println("the " + cardIdentity3 + " of " + cardSuit3);
    System.out.println("the " + cardIdentity4 + " of " + cardSuit4);
    System.out.println("the " + cardIdentity5 + " of " + cardSuit5);
      
    /////////////////////////////////////// Now we need to incorporate booleans, which will check the valdiity of our statements. Told by TA to start with three of a kind first
    // Also keep note, you cannot have more than two double equals signs in a line
    
    //booleans (primitive type) must be set equal to 0
    boolean highCardHand = false; //Used if the card is High card hand
    boolean twoOfKind = false; /// Boolean variable for a card pair
    boolean threeOfKind = false; /// Boolean variable for three of a kind
    
    //You need to use &&. Booleans require it and you will just save alot of time
    if (cardIdentity1==cardIdentity2 && cardIdentity1 == cardIdentity3){ //Wrote down all possibilities on external sheet of paper
      threeOfKind = true; } //Need to use else if statements from this point on. Finish all the possibillities that involve card 1
    else if (cardIdentity1 == cardIdentity2 && cardIdentity1 == cardIdentity4){
      threeOfKind = true; }
    else if (cardIdentity1 == cardIdentity2 && cardIdentity1 == cardIdentity5){
      threeOfKind = true; } //From now on, just follow this same patterm starting with the first card
    else if (cardIdentity1 == cardIdentity3 && cardIdentity1 == cardIdentity4){
      threeOfKind = true; } 
    else if (cardIdentity1 == cardIdentity3 && cardIdentity1 == cardIdentity5){
      threeOfKind = true; }
    else if (cardIdentity1 == cardIdentity4 && cardIdentity1 == cardIdentity5){
      threeOfKind = true; } //All Combinations starting with card 1 are done, now keep going with card 2!
    else if (cardIdentity2 == cardIdentity3 && cardIdentity2 == cardIdentity4){
      threeOfKind = true; }
    else if (cardIdentity2 == cardIdentity3 && cardIdentity2 == cardIdentity5){
      threeOfKind = true; } 
    else if (cardIdentity2 == cardIdentity4 && cardIdentity2 == cardIdentity5){
      threeOfKind = true; }
    else if (cardIdentity3 == cardIdentity4 && cardIdentity3 == cardIdentity5){
      threeOfKind = true; }
    //Every possible combination has been written out for the three of a kind
    
    //Last but not least, we have to use boolean comparisons for the two of a kind possibilities. You have to use the || (Logical OR) operator
    if (cardIdentity1 == cardIdentity2|| cardIdentity1 == cardIdentity3|| cardIdentity1 == cardIdentity4|| cardIdentity1 == cardIdentity5)
    {twoOfKind = true; } //All comparisons for card 1 are done
    else if (cardIdentity2 == cardIdentity3|| cardIdentity2 == cardIdentity4|| cardIdentity2 == cardIdentity5)
    {twoOfKind = true; } //All comparisons for card 2 are done
    else if (cardIdentity3 == cardIdentity4||cardIdentity3 == cardIdentity5)
    {twoOfKind = true; } //All comparisons for card 3 are done
    else if (cardIdentity4 == cardIdentity5)
    {twoOfKind = true; }
    
    //Now, we have to use comparisions to print out the results of our hands
    if (twoOfKind == true) {System.out.println("You have a pair! ");
                           } //For next line: != is equivalent to "not equal to"
    
    else if (threeOfKind = true) {System.out.println("You have three of a kind! ");
                                 }
    
    else if (twoOfKind != true && threeOfKind != true) {highCardHand = false;
      
      System.out.println("You have a high card hand. "); }
  
    
  }//End of main method
} //End of public class
 

    
    
    
      
    
 
    
      
    
    
    
    
    
    

