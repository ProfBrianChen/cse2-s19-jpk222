////////////// CSE 002 Jake Kassis
//// Sentence Generator
/// HW 07 3/26/19 Part 1
//Import java scanner which is necessary for the assignment
import java.util.Scanner;
public class Area{
  //Main method required for starting every java program
  public static void main(String[] args){
  //Use main method to prompt for information and print all of our results
  //Prompt for user to input string
    Scanner userInput = new Scanner(System.in);
    System.out.print("Please enter a shape (rectangle, triangle or circle): ");
    while(userInput.hasNext() == false){
      //String to discard wrong answer
      String junkInput = userInput.next();
      System.out.print("Error: Please provide answer in the form of a string: ");
    }
    //Store what is provided, it will be checked in outside method
    String shapeInput = userInput.next();
    
    //Now, check to see if the input is one of the three shape (Use an if statement)
    //Create strings for each shape
    String rectangle = "rectangle";
    String triangle = "triangle";
    String circle = "circle";
    //Repeat same process for each shape that we want
    if (shapeInput.equals(rectangle)){
      System.out.print("Please enter a height in double format: ");
      while(userInput.hasNextDouble() == false){
        String junkInput = userInput.next();
        System.out.print("Error: Please provide a height in double format: ");
        }
      double rectHeight = userInput.nextDouble();
      
      System.out.print("Please enter a width in double format: ");
      while(userInput.hasNextDouble() == false){
          String junkInput = userInput.next();
          System.out.print("Error: Please Provide a width in double format");
        }
      double rectWidth = userInput.nextDouble();
      Rectangle(rectHeight, rectWidth);
      }
    //For Triangle
    else if (shapeInput.equals(triangle)){
      System.out.print("Please enter a height in double format: ");
      while(userInput.hasNextDouble() == false){
        String junkInput = userInput.next();
        System.out.print("Error: Please provide a height in double format: ");
        }
      double triHeight = userInput.nextDouble();
      
      System.out.print("Please enter a width in double format: ");
      while(userInput.hasNextDouble() == false){
          String junkInput = userInput.next();
          System.out.print("Error: Please Provide a width in double format");
        }
      double triWidth = userInput.nextDouble();
      Triangle(triHeight, triWidth);
    }
  
    //For circle
    else if (shapeInput.equals(circle)){
      System.out.print("Please enter a radius in double format: ");
      while(userInput.hasNextDouble() == false){
        String junkInput = userInput.next();
        System.out.print("Error: Please provide a radius in double format: ");
      }
      double circRadius = userInput.nextDouble();
      Circle(circRadius);
      
      }
    
  }
  
  //Make an individual method for ecah of the areas we must calculate
  //Area of rectangle
  public static void Rectangle(double rectHeight, double rectWidth){
    double areaRectangle = ((rectHeight) * (rectWidth));
    System.out.println("The area of your rectangle is: " + areaRectangle);
    }
  //Area of a triangle
  public static void Triangle(double triHeight, double triWidth){
    double areaTriangle = ((triHeight) * (triWidth) * (.5));
    System.out.println("The area of your triangle is: " + areaTriangle);
    }
  //Area of a circle
  public static void Circle(double circRadius){
    double circleArea = ((circRadius) * (circRadius) * (3.14));
    System.out.println("The area of your circle is: " + circleArea);
  }
  
}


      

      
      
      
    
   
  
  
  
  
        
        
        
        
        
        
        
      