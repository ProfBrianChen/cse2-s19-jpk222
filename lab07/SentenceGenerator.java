////////////// CSE 002 Jake Kassis
//// Sentence Generator
//// Lab 07 3/25/19
import java.util.Random;
public class SentenceGenerator{
  //Main method as used for every java program
  public static void main(String[] args){
    //Use the main method to contain the sentence components
    
    
    String subject = SentenceSubject();
    System.out.print("The " + subject + SentenceVerb() + "The " + SentenceAdjective() + SentenceObject() +".");
    //System.out.println(SentenceVerb());
    //System.out.println("The " + SentenceAdjective());
    //System.out.println(SentenceObject());
    System.out.print(" " + ItGenerator(subject) + " clawed " + "The " + SentenceObject() + " and" + " the " + SentenceAdjective() + SentenceObject() + "." );
    System.out.print(" " + ItGenerator(subject) + " clawed " + "The " + SentenceObject() + " and" + " the " + SentenceAdjective() + SentenceObject() + "." );
  }
  
  //Create method for the Sentence Subject
  public static String SentenceSubject(){
    //Use random number generator, switch statement and then return the value
    Random randomGenerator = new Random();
    //Generate a random number less than 10
    int subjectInt = randomGenerator.nextInt(10);
    //String for subjectIdentity
    String subjectIdentity = "";
    //Switch statement for which subjects will be printed
    switch(subjectInt){
      case 0: subjectIdentity = "Dog ";
        break;
      case 1: subjectIdentity = "Lizard ";
        break; 
      case 2: subjectIdentity = "Wolf ";
        break; 
      case 3: subjectIdentity = "Fox ";
        break;
      case 4: subjectIdentity = "Sparrow ";
        break;
      case 5: subjectIdentity = "Moose ";
        break; 
      case 6: subjectIdentity = "Giraffe ";
        break;
      case 7: subjectIdentity = "Dinosaur ";
        break;
      case 8: subjectIdentity = "Man ";
        break;
      case 9: subjectIdentity = "Woman ";
        break; 
    }
        return subjectIdentity;
  }
  
  public static String ItGenerator(String subjectIdentity){
    Random randomGenerator = new Random();
    //Chance for sentence to print "It" or the subject first
    int subjectItInt = randomGenerator.nextInt(2);
    
    String itChance = "";
    //Create switch for the probability of the sentence beginning with "it"
    switch(subjectItInt){
      case 0: itChance = "It";
        break;
      case 1: itChance = subjectIdentity;
        break;
    }
    
    return itChance;
    
  }
  //Create method for the sentence subject
  public static String SentenceVerb(){ //Verb must be in past tense
    
    Random randomGenerator = new Random();
    
    int verbInt = randomGenerator.nextInt(10);
    
    String verbIdentity = "";
    
    switch(verbInt){
      case 0: verbIdentity = "Passed ";
        break; 
      case 1: verbIdentity = "Met ";
        break; 
      case 2: verbIdentity = "Jumped ";
        break; 
      case 3: verbIdentity = "Spurned ";
        break;
      case 4: verbIdentity = "Intimidated ";
        break; 
      case 5: verbIdentity = "Encountered ";
        break; 
      case 6: verbIdentity = "Greeted ";
        break; 
      case 7: verbIdentity = "Killed ";
        break;
      case 8: verbIdentity = "Save ";
        break;
      case 9: verbIdentity = "Comforted ";
        break;
    }
        
        return verbIdentity;
 }
  
  public static String SentenceAdjective(){
    
    Random randomGenerator = new Random();
    
    int adjectiveInt = randomGenerator.nextInt(10);
    
    String adjectiveIdentity = "";
    
    switch(adjectiveInt){
      case 0: adjectiveIdentity = "Small ";
        break; 
      case 1: adjectiveIdentity = "Quick ";
        break;
      case 2: adjectiveIdentity = "Fearsome ";
        break;
      case 3: adjectiveIdentity = "Lazy ";
        break;
      case 4: adjectiveIdentity = "Attractive ";
        break;
      case 5: adjectiveIdentity = "Interesting ";
        break; 
      case 6: adjectiveIdentity = "Majestic ";
        break; 
      case 7: adjectiveIdentity = "Knew ";
        break; 
      case 8: adjectiveIdentity = "Recognized ";
        break;
      case 9: adjectiveIdentity = "Acknowledged ";
        break; 
        
    }
        return adjectiveIdentity;
}
  
  public static String SentenceObject(){
    
    Random randomGenerator = new Random();
    
    int objectInt = randomGenerator.nextInt(10);
    
    String objectIdentity = "";
    
    switch(objectInt){
      case 0: objectIdentity = "Cat ";
        break;
      case 1: objectIdentity = "Mouse ";
        break;
      case 2: objectIdentity = "Boy ";
        break; 
      case 3: objectIdentity = "Girl ";
        break;
      case 4: objectIdentity = "Elderly Man ";
        break;
      case 5: objectIdentity = "Elderly Woman ";
        break;
      case 6: objectIdentity = "Ghost ";
        break; 
      case 7: objectIdentity = "Ghoul ";
        break; 
      case 8: objectIdentity = "Snake ";
        break;
      case 9: objectIdentity = "Snowman ";
    
    }
        return objectIdentity; 
    
    
  }      
}
  
  
  
  
  
  
  
    
    
    
      
    
    
    
    
    
    
    
    
    
  












